FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/toto.sh"]

COPY toto.sh /usr/bin/toto.sh
COPY target/toto.jar /usr/share/toto/toto.jar
